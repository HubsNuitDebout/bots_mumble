


<h1>Pour utiliser un bot il faut creer se creer une clef et un certificat :</h1>


openssl genrsa -out key.pem 2048

openssl req -new -x509 -days 3650 -key key.pem -out cert.pem




<h1>Projets</h1>


objectif : Permettre de créer/administrer/moderer des salons de façon autonome pour les utilisateurs de mumble

<h2>/creationSalon </h2> 

Affiche un message de bienvenue
Démarre le questionnement de l’utilisateur

	1. Quel type de Salon souhaitez-vous ?
		1.1 Privé
		1.2 Public
	2. Thématique abordée ?
		2.1 Démocratie
		2.2 Santé
		2.3 Sociale
		2.4 Politiques Etrangères
		2.5 Numérique
		2.6 Transmissions des savoirs
		2.7 Economie
		2.8 Medias
		2.9 Environnement
	3. Souhaitez-vous préciser le sujet ?
		3.1 Oui
			3.1.1 Champ de saisi :
		3.2 Non
	4. Faites-vous une retransmission audio ?
		4.1 Reporter ?
		4.2 Commission ?
			4.2.1 Indiquez votre ville
		4.3 Assemblée générale ?
			4.3.1 Indiquez votre ville
	5. Souhaitez-vous invitez des amis ?
	6. Souhaitez-vous limiter le nombre de participant ?
	7. Souhaitez-vous ecrire une description pour votre salon ?


<h2>/editDescription</h2>

	Modifier la description

	Return command :
		
		Message de confirmation
		Mise à jour Description avec les règles en place dans ce salon et la description souhaité

<h1>Admin_bot :</h1>

<h2>/configuration </h2>

	Affichage numéroté des options d’administrations et status de chaque
		0 / refuser 1 / accorder 

	1.Entrer = 1
	2.Parler = 1
	3.Chuchoter = 0
	4.chat = 1 

<h2>/ajoutModerateur <i>nomUtilisateur</i></h2>

	Ajouter un moderateur du salon

	Return command :
	
		Message de confirmation
		Ajouter le nom dans la liste de moderation présente dans la description du salon
 
<h2>/enleverModerateur <i>nomUtilisateur</i></h2>

	Enlever un moderateur du salon

	Return command :
		
		Message de confirmation
		Supprimer le nom dans la liste de moderation présente dans la description du salon


<h1>Mod_bot :</h1>

<h2>/autoriserUtilisateur <i>nomUtilisateur</i> </h2>

	Ajouter un participant dans le groupe d’autorisation pour entrer dans le salon 

	Return command :

		Message de confirmation

<h2>/exclureUtilisateur <i>nomUtilisateur</i></h2>

	Exclut le participant du salon et le supprime de la liste des autorisés à entrer

	Return command :

		Message de confirmation

<h2>/muetUtilisateur <i>nomUtilisateur</i></h2> 

<h2>/paroleUtilisateur <i>nomUtilisateur</i></h2>


<h1>Info_bot :</h1>

<h2>/infoAll</h2>

	Affiche la description du canal
	Affiche les règles du canal
	Affiche la liste des modérateurs

<h2>/listMod</h2>

	Affiche la liste des modérateurs

<h2>/description</h2>

	Affiche la description du canal

<h2>/regles</h2>

	Affiche les règles du canal
