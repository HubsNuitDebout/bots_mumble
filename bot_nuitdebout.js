/* jslint node: true */
"use strict";

var mumble = require('mumble');
var fs = require('fs');
var wav = require('wav');
var lame = require('lame');
var mumbleServer = "mumble.nuitdebout.org";
var botUsername = "compagnion_bot";

var options = {};
try {
  options = {
    key: fs.readFileSync('key.pem'),
    cert: fs.readFileSync('cert.pem')
  };
} catch (e) {
  console.log('Could not load private/public certificate files.');
  console.log('Trying to connect without client certificate.');
}

var tree = "";
var speaking = [];
var speachtimer;
var followeduser;
var speakers = {};

function buildChannelTree(channel, level) {
  for (var i = 0; i < level; i++) {
    tree += "   ";
  }
  tree += "  - " + channel.name + ": ";
  for (var u in channel.users) {
    var user = channel.users[u];
    tree += user.name + ", ";
  }
  tree += "\n";
  for (var c in channel.children) {
    buildChannelTree(channel.children[c], level + 1);
  }
}

console.log('Connecting');
mumble.connect(mumbleServer, options, function(error, connection) {
  if (error) {
    throw new Error(error);
  }
  console.log('Connected');
  connection.on('ready', function() {
    console.log("Ready!");
    buildChannelTree(connection.rootChannel, 0);
    console.log(tree);
    console.log("Those were all channels!");
    console.log("Users:");
    var list = connection.users();
    for (var key in list) {
      var user = list[key];
      console.log("  - " + user.name + " in channel " + user.channel.name);
    }
    console.log("\nThose were all users!");
  });
  connection.on('channel-move', function(channel, from, to) {
    console.log("Channel " + channel.name + " was moved from " + from.name + " to " + to.name);
  });
  connection.on('channel-links-add', function(channel, links) {
    for (var key in links) {
      console.log("Channel " + links[key].name + " was linked to " + channel.name);
    }
  });
  connection.on('channel-links-remove', function(channel, links) {
    for (var key in links) {
      console.log("Channel " + links[key].name + " was unlinked from " + channel.name);
    }
  });
  connection.on('channel-rename', function(channel, oldName, newName) {
    console.log("Channel " + oldName + " was renamed to " + newName);
  });
  connection.on('user-mute', function(user, muted, actor) {
    console.log("User " + user.name + " changed mute to: " + muted + " by " + actor.name);
  });
  connection.on('user-self-deaf', function(user, deaf, actor) {
    console.log("User " + user.name + " changed deaf to: " + deaf + " by " + actor.name);
  });
  connection.on('user-self-mute', function(user, muted, actor) {
    console.log("User " + user.name + " changed self-mute to: " + muted + " by " + actor.name);
  });
  connection.on('user-suppress', function(user, suppress, actor) {
    console.log("User " + user.name + " changed suppress to: " + suppress + " by " + actor.name);
  });
  connection.on('user-move', function(user, fromChannel, toChannel, actor) {
    if (followeduser) {
      if (user.name == followeduser) {
        toChannel.join();
      }
    }
    console.log("User " + user.name + " moved from channel " + fromChannel.name + " to " + toChannel.name + " by " + actor.name);
  });
  connection.on('user-recording', function(user, state, actor) {
    console.log("User " + user.name + (state ? ' started' : ' stopped') + " recording" + " by " + actor.name);
  });
  connection.on('user-disconnect', function(user) {
    console.log("User " + user.name + " disconnected");
  });
  connection.on('user-connect', function(user) {
    console.log("User " + user.name + " connected");
  });
  connection.on('channel-create', function(channel) {
    console.log("Channel " + channel.name + " created");
  });
  connection.on('channel-remove', function(channel) {
    console.log("Channel " + channel.name + " removed");
  });
  connection.on('message', function(message, actor) {
    if (handleCommand(message, actor, connection)) {
      return;
    }

    //actor.sendMessage("I received: '" + message + "'");
    //connection.user.channel.sendMessage("I received: '" + message + "'");
  });
  connection.on('voice-start', function(user) {
    console.log('User ' + user.name + ' started voice transmission');
    speakers[user.name] = setTimeout(function() {
      // console.log('toto')
      connection.user.channel.sendMessage("(╯°□°）╯︵ ┻━┻ Je vous demande de vous areter " + user.name + " !!!");
    }, 30000);
    speaking.push(user);
    if (speachtimer) {
      clearTimeout(speachtimer);
    }
  });
  connection.on('voice-end', function(user) {
    console.log('User ' + user.name + ' ended voice transmission');
    if (speakers[user.name]) {
      clearTimeout(speakers[user.name]);
    }
    // speaking.pop(user)
    // if(speaking.length == 0){
    //   speachtimer = setTimeout(function(){
    //     // console.log('toto')
    //     connection.user.channel.sendMessage("(╯°□°）╯︵ ┻━┻");
    //   }, 7000);
    // }
  });
  connection.authenticate(botUsername);
});

function checktitle(strs){

    var re = /([^a-z A-Z 0-9 \s]+)/;
    var str = strs;
    var m;
    if ((m = re.exec(str)) !== null) {
      if (m.index === re.lastIndex) {
        re.lastIndex++;
      }

    return m[0];

    }
    return true;
}


function stringArrayToHTML(context,array) {

  var bufferString ="";

    for (var i = 0; i < array.length; i++) {

      var tmp = array[i];
      bufferString += "<br>"+tmp ;

    }
context.connection.user.channel.sendMessage(bufferString);
}

/*

      Bloc avec toutes les commandes


      !help : Affiche le descriptif des commandes
      !channelInfos nomDuChannel : Affiche les LCA qui régissent le salon
      !create nomDuSalonParent/nomDuSalon : Créé ton salon
      !follow nomUtilisateur : Dire au bot de suivre un utilisateur
      !gniagnia : Test d'enregistrement du canal en mp3
      !join nomDuSalon : Rejoindre un salon
      !msg nomUtilisateur1,nomUtilisateur2,... message : permet d'ecrire à plusieurs utilisateurs à travers le bot
      !remove nomDuSalon :Supprimer un salon
      !unfollow nomUtilisateur : Annuler le suivi

*/

var commands = [
  {
    command: /!help/,
    action: function(context) {

var help = [
      "<b>!help</b> : Affiche le descriptif des commandes",
      "<b>!channelAdminInfos</b> nomDuChannel : Affiche les LCA qui régissent le salon",
      "<b>!create</b> nomDuSalonParent/nomDuSalon : Créé ton salon ",
      "<b>!follow</b> nomUtilisateur : Dire au bot de suivre un utilisateur",
      "<b>!gniagnia</b> : Test d'enregistrement du canal en mp3 ",
      "<b>!join</b> nomDuSalon : Rejoindre un salon ",
      "<b>!msg</b> nomUtilisateur1,nomUtilisateur2,... message : permet d'ecrire à plusieurs utilisateurs à travers le bot",
      "<b>!remove</b> nomDuSalon :Supprimer un salon ",
      "<b>!unfollow</b> nomUtilisateur : Annuler le suivi",
      ];


      stringArrayToHTML(context,help);


     }
  },  {
    command: /!channelAdminInfos (.*)/,
    action: function(context, channelName) {
      var channel = context.connection.channelByName(channelName);
      if (!channel) {
        return context.actor.sendMessage('Unknown channel: ' + channelName);
      }

      channel.getPermissions(function(err, permissions) {
        if (err) {
          return context.actor.sendMessage('Error: ' + err);
        }
        context.actor.sendMessage(channelName + ' permissions: ' + JSON.stringify(permissions));
      });
    }
  }, {
    command: /!msg ([^:]+): (.*)/,
    action: function(context, names, message) {

      var recipients = {
        session: [],
        channel_id: []
      };

      names = names.split(',');
      for (var n in names) {
        var name = names[n];

        var user = context.connection.userByName(name);
        if (user) {
          recipients.session.push(user.session);
          console.log(user.session);
        }

        var channel = context.connection.channelByName(name);
        if (channel) {

          recipients.channel_id.push(channel.id);
        }
      }

      context.connection.sendMessage(message, recipients);
    }
  }, {

    command: /!create (.*)\/(.*)/,
    action: function(context, parent, name) {

      var existChannel = context.connection.channelByName(name);
//      console.log("existChannel : " + existChannel) ;
        if(existChannel === undefined){

          var bool1 = checktitle(name);

          if (bool1 === true){
            // name good
            parent = context.connection.channelByName(parent);
            parent.addSubChannel(name);

          }else{
           // name erreur
           var m = bool1;
           console.log("erreur remove:"+m);
           context.connection.user.channel.sendMessage("erreur , les caracteres speciaux ne sont pas accepté . le caractere :"+m+" à été trouvé");
          return ;
          }

        }else{

          var errMsg = "le salon existe déjà" ;
          console.log(errMsg);
          context.connection.user.channel.sendMessage(errMsg);

        }
    }
  }, {
    command: /!remove\s(.*)/,
    action: function(context, name) {

          var bool1 = checktitle(name);

          if (bool1 === true){
            var channel = context.connection.channelByName(name);
            channel.remove();
          }else{
           var m = bool1;
           console.log("erreur remove:"+m);
           context.connection.user.channel.sendMessage("erreur , les caracteres speciaux ne sont pas accepté . le caractere :"+m+" à été trouvé");
           return ;

          }

    }
  }, {
    command: /!join (.+)/,
    action: function(context, name) {

      var channel = context.connection.channelByPath(name);
      if (channel) {
        channel.join();
      }
    }
  }, {
    command: /!follow (.+)/,
    action: function(context, name) {

      followeduser = name;
      context.connection.user.channel.sendMessage("target aquired, following");

    }
  }, {
    command: /!unfollow/,
    action: function(context) {

      followeduser = "";
      //context.connection.user.channel.sendMessage("target lost");

    }
  }, {
    command: /!gniagnia/,
    action: function(context) {


      var input = context.connection.inputStream();
      var decoder = new lame.Decoder();
      var inputfile = fs.createReadStream('./test.mp3');

      var stream;
      decoder.on('format', function(format) {
        console.log(format);

        stream.pipe(context.connection.inputStream({
          channels: format.channels,
          sampleRate: format.sampleRate,
          gain: 0.25
        }));
      });

      stream = inputfile.pipe( decoder );
    }
  },{
    command: /!wav/,
    action: function(context) {

          var input = context.connection.inputStream();
          var file = fs.createReadStream('test_sounds/test.wav');
          var reader = new wav.Reader();

          var stream;
          reader.on( 'format', function( format ) {
              console.log( format );

              reader.pipe( context.connection.inputStream({
                  channels: format.channels,
                  sampleRate: 15000,
                  gain: 0.25
              }));
          });
          stream = file.pipe(reader);
    }
  },{
    command: /!mp3/,
    action: function(context) {

    var input = context.connection.inputStream();
    var file = fs.createReadStream('test_sounds/Bonjour.mp3');
    var decoder = new lame.Decoder();

    var stream;
    decoder.on( 'format', function( format ) {
        console.log( format );

        stream.pipe( context.connection.inputStream({
            channels: format.channels,
            sampleRate: format.sampleRate,
            gain: 0.25
        }));
    });
    stream = file.pipe(decoder);
    }

  }
];

var handleCommand = function(message, actor, connection) {
  for (var c in commands) {
    var cmd = commands[c];

    var match = cmd.command.exec(message);
    if (!match) {
      continue;
    }

    var params = match.slice(1);
    params.unshift({
      message: message,
      actor: actor,
      connection: connection
    });
    cmd.action.apply(null, params);

    return true;
  }
};
